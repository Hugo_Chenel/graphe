#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pprint import pprint
import numpy as np  # for Floyd-Warshall matrices


# Graph manipulation functions
##############################

def create_graph(directed=True, weighted=False):  # TP1
	"""
	create a dictionnary representing a graph and returns it.
	"""
	g = {'nodes': {}, 'edges': {}, 'nb_edges': 0, 'directed': directed, 'weighted': weighted, 'weight_attribute': None}
	return g


def add_node(g, n, attributes=None):  # TP1
	"""
	add a node n (node id provided as a string or int) to the graph g.
	attributes on the node can be provided by a dict.
	returns the node n attributes.
	"""
	if n not in g['nodes']:  # ensure node does not already exist
		if attributes is None:  # create empty attributes if not provided
			attributes = {}
		g['nodes'][n] = attributes
		g['edges'][n] = {}  # init outgoing edges
	return g['nodes'][n]  # return node attributes


# pour ajouter un arc, il faut vérifier que l'extrémité initiale et terminale existent, et que l'arc n'existe pas. Puis ajouter une condition dans le cas où le graphe n'est pas orienté.
def add_edge(g, n1, n2, attributes=None, n1_attributes=None, n2_attributes=None):  # TP1
	# create nodes if they do not exist
	if n1 not in g['nodes']: add_node(g, n1, n1_attributes)  # ensure n1 exists
	if n2 not in g['nodes']: add_node(g, n2, n2_attributes)  # ensure n2 exists
	# add edge(s) only if they do not exist
	if n2 not in g['edges'][n1]:
		if attributes is None:  # create empty attributes if not provided
			attributes = {}
		g['edges'][n1][n2] = attributes
		if not g['directed']:  # Pour un graphe non orienté
			g['edges'][n2][n1] = g['edges'][n1][n2]  # share the same attributes as n1->n2
		g['nb_edges'] += 1
	return g['edges'][n1][n2]  # return edge attributes


# crée un graphe (non) orienté
def load_SIF(filename, directed=True):  # TP1 - permet de lire le fichier ligne par ligne et ajouter /arcs
	"""
	parse a SIF (cytoscape Simple Interaction Format) file and returns a directed graph.
	line syntax: nodeD <relationship type> nodeE nodeF nodeB
	"""
	g = create_graph(directed)  # new empty graph
	with open(filename) as f:  # OPEN FILE
		# PROCESS THE REMAINING LINES
		row = f.readline().rstrip()  # read next line and remove ending whitespaces
		while row:
			vals = row.split('\t')  # split line on tab
			for i in range(2, len(vals)):
				att = {'type': vals[1]}  # set edge type
				add_edge(g, vals[0], vals[i], att)
			row = f.readline().rstrip()  # read next line
	return g  # return created graph


def load_TAB(filename, directed=True, weighted=False, weight_attribute=None):  # TP3
	"""
	parse a TAB file (as cytoscape format) and returns a graph.

	line syntax: id1	id2	att1	att2	att3	...
	"""
	g = create_graph(directed, weighted)
	with open(filename) as f:
		# GET COLUMNS NAMES
		tmp = f.readline().rstrip()
		attNames = tmp.split('\t')
		# REMOVES FIRST TWO COLUMNS WHICH CORRESPONDS TO THE LABELS OF THE CONNECTED VERTICES
		attNames.pop(0)
		attNames.pop(0)
		# PROCESS THE REMAINING LINES
		row = f.readline().rstrip()
		while row:
			vals = row.split('\t')
			u = vals.pop(0)
			v = vals.pop(0)
			att = {}
			for i in range(len(attNames)):
				att[attNames[i]] = vals[i]
			add_edge(g, u, v, att)
			row = f.readline().rstrip()  # NEXT LINE
		return g


def get_nodes(g):  # liste des noms des sommets
	return list(g['nodes'].keys())


def get_edge_attribute(g, src, dst, attribut_names):
	# attributs des arcs de source vers destination
	return g[keys][src][dst][attribut_names]


def get_neighbors(g, n):  # liste des sommets adjacents
	return list(g['edges'][n].keys())


# Parcours en largeur
def BFS(g, s):  # graphe g et sommet s
	color = dict()
	d = dict()
	Pi = dict() #correspond au prédécesseur
	for u in get_nodes(g):
		color[u] = 'WHITE'
		d[u] = float('inf')  # +infinie
		Pi[u] = None
	Pi[s] = None
	color[s] = 'GREY'
	d[s] = 0
	Q = []
	Q.append(s)
	while len(Q) != 0:  # tant que Q n'est pas vide
		u = Q.pop(0)  # retire le premier élément (défilement de Q)
		for v in get_neighbors(g, u):  # pour chaque successeur
			if color[v] == 'WHITE':  # si le successeur n'est pas marqué
				color[v] = 'GREY'  # on marque le successeur
				d[v] = d[u] + 1  # la distance du successeur prend la valeur de la distance de u + 1
				Pi[v] = u
				Q.append(v)
		color[u] = 'BLACK'
	return (Pi, color, d)


# Parcours en profondeur
def DFS(g):
	global time
	time = 0
	color = {}
	d = {}
	predecessor = {}
	classe = {}
	f = {}
	def DFS_visit(u):
		global time
		color[u] = 'GRAY' #le sommet vient d'être découvert
		time += 1
		d[u] = time
		for v in get_neighbors(g,u): #on obtient les différents cas selon l'état du voisin : a-t-il dejà été visité ou non?
			if color[v] == 'WHITE':
				predecessor[v] = u
				DFS_visit(v)
				classe[(u,v)] = 'TREE EDGE'
			elif color[v] == 'GRAY':
				classe[(u,v)] = 'BACK EDGE'
			elif d[u] > d[v]:
				classe[(u,v)] = 'CROSS EDGE'
			else:
				classe[(u,v)] = 'FORWARD EDGE'
		color[u] = 'BLACK' #lorsque le sommet est noir, cela signifie que c'est terminé
		time += 1
		f[u] = time
	for u in get_nodes(g):
		color[u] = 'WHITE' #on marque le sommet
		predecessor[u] = None
	for u in get_nodes(g):
		if color[u] == 'WHITE': #si le successeur n'est pas marqué
			DFS_visit(u) #on marque le successeur à l'aide de la fonction DFS_visit
	return f, time, color, d, predecessor, classe

def is_acyclic(g):
	f, time, color, d, predecessor, classe = DFS(g)
	return 'BACK EDGE' not in classe.values()


def topological_sort(g):
	f, time, color, d, predecessor, classe = DFS(g)
	return [(k, f[k]) for k in sorted(f, key=f.get, reverse=True)]



def weight(g, u, v, w):
	return float(g['edges'][u][v][w])

def INITIALIZE(g, s):
	d = {}
	n = {}
	for v in get_nodes(g):
		d[v] = float('inf')
		n[v] = None
	d[s] = 0
	return d, n


def RELAX(g, d, n , u, v, w): #la fonction weight est appelée dans cette fonction RELAX
	if d[v] > (d[u] + weight(g, u, v, w)):
		d[v] = (d[u] + weight(g, u, v, w))
		n[v] = u
	return d, n


def Bellman_Ford(g, s, w):
	d, n = INITIALIZE(g, s)
	for i in range(len(get_nodes(g)) - 1): #boucle sur l'ensemble des sommets du graphe
		for u in get_nodes(g):
			for v in g['edges'][u]:
				RELAX(g, d, n, u, v, w)
	return d, n




##### main / #####
if __name__ == "__main__":
	# ~ test_graph_manipulation()
	print("Graph lib tests")
	g = create_graph()
	# add_node(g, 'A', {'alias': 'grand A', 'size': 58.5})
	# add_edge(g, 'A', 'B', {'weight': 5})
	print(g)

	g = load_SIF('Graphe_dressing.sif')

	# ~ test_BFS()
	print("# Affichage de BFS à partir de sous-vetements : ")
	g_BFS = BFS(g, 'sous-vetements')
	pprint(g_BFS)
	# print("Dressing_BFS \n OK \n")
	# pprint(G_BFS))

	# ~ test_DFS()
	print("# Affichage de DFS : ")
	g_DFS = DFS(g)
	pprint(g_DFS)

	# Graphe est sans circuit ou non
	print("# Le graphe contient un circuit ? ")
	IA = is_acyclic(g)
	pprint(IA)

	# Renvoie les sommets du graphes en ayant effectué un tri topologique
	print("# Sommets du graphe triés topologiquement : ")
	TS = topological_sort(g)
	pprint(TS)



	B = load_TAB('Graphe_Bellman-Ford.tab')

	# ~ test_BellmanFord()
	print("# Affichage du plus court chemin à l'aide de Bellman-Ford : ")
	pprint(B)
	BF = Bellman_Ford(B, 'A', 'weight')
	pprint(BF)